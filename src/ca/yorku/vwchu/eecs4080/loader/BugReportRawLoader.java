/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.loader;
import ca.yorku.vwchu.eecs4080.structs.*;
import org.joox.*;
import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.stream.*;
import static ca.yorku.vwchu.eecs4080.util.DateUtil.*;
import static org.joox.JOOX.*;

/**
 * Loader for raw bug reports.
 * Reads and populates the Bundle representing
 * a bug report from the raw XML bug report file.
 */
public class BugReportRawLoader {

    /**
     * Loads the file corresponding to the given
     * filename. Builds and populates the Bundle
     * representing a bug report from the raw XML
     * bug report file.
     *
     * @param filename
     *      the file path to raw bug report file
     * @return
     *      the newly created bundle object
     * @throws IOException
     *      if IO error occurs
     */
    public static Bundle load(String filename) throws IOException {
        return load($("<?xml version='1.0' encoding='utf-8'?>" +
            Files.lines(
                FileSystems.getDefault().getPath(filename),
                StandardCharsets.UTF_8
            ).collect(Collectors.joining(System.lineSeparator()))
                .replaceAll("[&]#8220;", "\"")
                .replaceAll("[&]#8221;", "\"")
                .replaceAll("[&]#8217;", "\'")
        ).find("rss > channel > item"));
    }

    // MARK: - private static Helpers

    /**
     * Creates and populates a bundle given a Match
     * object from the bug report XML parser (JOOX). Represents
     * a bug report object and the corresponding fields.
     *
     * @param match
     *      XML parser (JOOX) match object
     * @return
     *      newly created bundle object
     */
    @SuppressWarnings("unchecked")
    private static Bundle load(Match match) {
        return (new Bundle())
            .assign("phase", 0)
            .assign("key project title link type priority status resolution environment",
                tag -> match.find(tag).text())
            .assign("reporter assignee",
                tag -> username(match.find(tag).attr("username")))
            .assign("created updated resolved due",
                tag -> datetime(match.find(tag).text()))
            .assign("votes watches",
                tag -> Integer.parseInt(match.find(tag).text()))
            .assign("description summary",
                tag -> match.find(tag).text())
            .assign("comments", BugReportRawLoader::makeComment,
                streamMatches(match.find("comments > comment")))
            .assign("attachments", BugReportRawLoader::makeAttachment,
                streamMatches(match.find("attachments > attachment")))
            .assign("subtasks", BugReportRawLoader::makeSubtask,
                streamMatches(match.find("subtasks > subtask")))
            .assign("issues", BugReportRawLoader::makeIssue,
                streamMatches(match.find("issuelinks > issuelinktype")).flatMap(m -> Stream.concat(
                    streamMatches(m.find("outwardlinks > issuelink > issuekey"))
                        .map(mm -> mm.attr("type", m.find("name").text())
                                     .attr("link_type", "outward")),
                    streamMatches(m.find("inwardlinks > issuelink > issuekey"))
                        .map(mm -> mm.attr("type", m.find("name").text())
                                     .attr("link_type", "inward")))));
    }

    /**
     * Returns the username given the string
     * representation: username or -1 for none.
     *
     * @param username
     *      to convert to username or null
     * @return
     *      username or null
     */
    private static String username(String username) {
        return "-1".equals(username) ? null : username;
    }

    /**
     * Returns a stream of match objects for the
     * given match object.
     *
     * @param match
     *      XML parser (JOOX) match object
     * @return
     *      stream of match objects
     */
    private static Stream<Match> streamMatches(Match match) {
        return match.each().stream();
    }

    /**
     * Creates and populates a bundle given a Match
     * object from the bug report XML parser (JOOX). Represents
     * a specific comment and the corresponding fields.
     *
     * @param match
     *      XML parser (JOOX) match object
     * @return
     *      newly created bundle object for a comment
     */
    private static Bundle makeComment(Match match) {
        return (new Bundle())
            .assign("id", match.attr("id"))
            .assign("author", match.attr("author"))
            .assign("created", datetime(match.attr("created")))
            .assign("content", match.text());
    }

    /**
     * Creates and populates a bundle given a Match
     * object from the bug report XML parser (JOOX). Represents
     * a specific attachment and the corresponding fields.
     *
     * @param match
     *      XML parser (JOOX) match object
     * @return
     *      newly created bundle object for an attachment
     */
    private static Bundle makeAttachment(Match match) {
        return (new Bundle())
            .assign("id", match.attr("id"))
            .assign("name", match.attr("name"))
            .assign("size", match.attr("size"))
            .assign("author", match.attr("author"))
            .assign("created", datetime(match.attr("created")));
    }

    /**
     * Creates and populates a bundle given a Match
     * object from the bug report XML parser (JOOX). Represents
     * a specific subtask and the corresponding fields.
     *
     * @param match
     *      XML parser (JOOX) match object
     * @return
     *      newly created bundle object for a subtask
     */
    private static Bundle makeSubtask(Match match) {
        return (new Bundle())
            .assign("id", match.attr("id"))
            .assign("content", match.text());
    }

    /**
     * Creates and populates a bundle given a Match
     * object from the bug report XML parser (JOOX). Represents
     * a specific issue and the corresponding fields.
     *
     * @param match
     *      XML parser (JOOX) match object
     * @return
     *      newly created bundle object for an issue
     */
    private static Bundle makeIssue(Match match) {
        return (new Bundle())
            .assign("id", match.attr("id"))
            .assign("type", match.attr("type"))
            .assign("link_type", match.attr("link_type"))
            .assign("content", match.text());
    }

} // BugReportRawLoader
