/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.transform;
import ca.yorku.vwchu.eecs4080.classifier.*;
import ca.yorku.vwchu.eecs4080.structs.*;
import org.joox.*;
import java.util.*;
import java.util.function.*;
import static ca.yorku.vwchu.eecs4080.util.CollectionUtil.*;
import static org.joox.JOOX.*;

/**
 * The phase 1 transformation for the entire bug
 * report, specifies the fields that need to be
 * transformed: phase, description, summary and
 * comments.content fields.
 *
 * Transforms the bundle, at phase 0 to phase 1,
 * converts the comments, description and summary
 * markup text into words and technical words.
 */
public class BugReportPhase1Transform {

    /**
     * Transforms the given bundle that represents
     * an entire bug report, at phase 0 to phase 1,
     * converts the comments, description and summary
     * markup text into words and technical words.
     *
     * @param bundle
     *      bundle that represents an entire bug report
     * @return
     *      the given bundle transformed
     */
    public static Bundle transform(Bundle bundle) {
        return BaseBugReportTransform.<String, Bundle>transform(bundle, 1, field ->
            (new Bundle())
                .assign("text", field)
                .assign("delimFn", "text", BugReportPhase1Transform::delimiter)
                .assign("xmldom", "text", BugReportPhase1Transform::makeXML)
                .manipulate("annotated", "text", BugReportPhase1Transform::annotateText)
                .manipulate("words", "annotated", BugReportPhase1Transform::tokenizeWords)
                .manipulate("annotated", "words", BugReportPhase1Transform::technicalWordsPass1)
                .manipulate("annotated", "words", BugReportPhase1Transform::technicalWordsPass2)
        );
    }

    /// MARK: - Private Helpers

    /**
     * Returns the a function that computes
     * the delimiter for the given string and
     * returns the character.
     *
     * @param text
     *      the original text
     * @return
     *      delimiter function
     */
    private static Function<String, Character> delimiter(String text) {
        final char minChar = (char) (text.chars().reduce(1000, Integer::max) + 1);
        final Map<String, Character> delims = new HashMap<>();

        return (name) -> {
            if (!delims.containsKey(name)) {
                delims.put(name, (char)(minChar + delims.size()));
            }
            return delims.get(name);
        };
    }

    /**
     * Constructs XML (HTML) DOM Match object
     * that represents the given text string.
     *
     * @param text
     *      the original text
     * @return
     *      XML DOM Match object
     */
    private static Match makeXML(String text) {
        try {
            return $("<?xml version='1.0' encoding='utf-8'?>" +
                    "<html><body>" + text.replaceAll("[&]nbsp;", "&#160;") +
                    "</body></html>").find("body").children();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Text: " + text);
            e.printStackTrace(System.err);
            return null;
        }
    }

    /**
     * Replaced XML tags with a character in the given string
     * with symbol and de-entityifies XML entities. Then, the
     * words are each prefixed and postfixed with symbols.
     *
     * @param text
     *      the original text
     * @param b
     *      reference to the bundle object
     * @return
     *      annotated string
     */
    private static String annotateText(String text, Bundle b) {
        Function<String, Character> delim = b.value("delimFn");

        return text
            .replaceAll("<[^<>]*/>", "" + delim.apply("self_tag"))
            .replaceAll("</[^<>]*>", "" + delim.apply("end_tag"))
            .replaceAll("<[^<>]*>", "" + delim.apply("start_tag"))
            .replaceAll("[&]gt;", ">")
            .replaceAll("[&]lt;", "<")
            .replaceAll("[&](apos|#8217);", "'")
            .replaceAll("[&](quot|#8220|#8221);", "\"")
            .replaceAll("[&](nbsp|#160);", " ")
            .replaceAll("[&](#8211);", "-")
            .replaceAll("[&]amp;", "&")
            .replaceAll(
                // match the words, including the symbols inside,
                // except the symbols prefixing and postfixing
                // (boundary) the word
                String.format(
                    "(%1$s|\\b)([%2$s]+(['.,:]+[%2$s]+)*)(%1$s|\\b)",
                    String.format("[%1$c%2$c%3$c]",
                        delim.apply("self_tag"),
                        delim.apply("start_tag"),
                        delim.apply("end_tag")),
                    "a-zA-Z0-9_\\-\\$#/%@&~^\\+\\*\\\\"),
                String.format("$1%c$2%c$4",
                    delim.apply("start"), delim.apply("end"))
            );
    }

    /**
     * Tokenizes the words in the given string into a map of
     * indices to words. Cycle through the characters
     * and for each substring between a start symbol and
     * end symbol, adds to the map.
     *
     * @param text
     *      the original text
     * @param b
     *      reference to the bundle object
     * @return
     *      map of indices to words
     */
    private static Map<Integer, String> tokenizeWords(String text, Bundle b) {
        Function<String, Character> delim = b.value("delimFn");
        char start = delim.apply("start");
        char end = delim.apply("end");
        StringBuilder temp = new StringBuilder();
        Map<Integer, String> words = new LinkedHashMap<>();

        forEachWithIndex(text.chars().mapToObj(i -> (char) i), (i, c) -> {
            if (c == start) {
                if (temp.length() > 0 && temp.charAt(0) == start) {
                    throw new AssertionError("misplaced start char");
                }
                temp.setLength(0);
                temp.append(c);
            } else if (c == end) {
                if (temp.length() > 0 && temp.charAt(0) != start) {
                    throw new AssertionError("misplaced end char");
                }
                words.put(i - temp.length(), temp.substring(1));
                temp.setLength(0);
            } else {
                temp.append(c);
            }
        });
        return words;
    }

    /**
     * Technical word identification, Pass 1.
     *
     * Annotates the annotated string with the given words. Flags
     * the words as possible technical words using the spell-checker.
     * Replaces the start and end annotation symbols with different
     * symbols to denote that they are flagged as technical words.
     * Returns re-annotated string.
     *
     * @param words
     *      map of indices to words
     * @param b
     *      reference to the bundle object
     * @return
     *      re-annotated string
     */
    private static String technicalWordsPass1(Map<Integer, String> words, Bundle b) {
        Function<String, Character> delim = b.value("delimFn");
        String text = b.value("annotated");

        char[] chars = text.toCharArray();

        // Filters the words by testing each against three
        // spell-checker. Those remaining, changes the start
        // and end symbol with different symbol (shifted
        // two characters).
        words.entrySet().stream()
            .filter(entry -> {
                int technical = 0;
                String v = entry.getValue();
                if (v.length() > 1) {
                    if (HunspellClassifier.test(v)) {technical++;}
                    if (JorthoClassifier.test(v)) {technical++;}
                    if (JazzyClassifier.test(v)) {technical++;}
                }
                return technical >= 2;
            })
            .forEachOrdered(entry -> {
                chars[entry.getKey()] = delim.apply("t_start");
                chars[entry.getKey() + entry.getValue().length() + 1] =
                    delim.apply("t_end");
            });

        return new String(chars);
    }

    /**
     * Technical word identification, Pass 2.
     *
     * Annotates the annotated string with the given words that are
     * flagged as possible technical words. Flags words that are
     * camel case, programming language keywords, or have special
     * characters in them. Replaces the start and end annotation
     * symbols with different symbols to denote that they are
     * flagged as true positive (or false negative -- not possible)
     * technical words. Returns re-annotated string.
     *
     * @param words
     *      map of indices to words
     * @param b
     *      reference to the bundle object
     * @return
     *      re-annotated string
     */
    private static String technicalWordsPass2(Map<Integer, String> words, Bundle b) {
        final String text = b.value("annotated");
        final Function<String, Character> delim = b.value("delimFn");
        final char t_start = delim.apply("t_start");

        char[] chars = text.toCharArray();

        //
        // Filters for the words flagged as technical words,
        // and then filters out the words that are programming
        // language keywords, camel cased, and contain special
        // characters. The remaining words are flagged as true
        // positive (or false negative -- not possible because
        // first filter) technical words.
        //
        // Comment out the filter for the words flagged as
        // technical words, to enable false negatives.
        //
        words.entrySet().stream()
            .filter(entry ->
                KeywordClassifier.predicate(b.value("text"))
                    .or(CamelCaseClassifier.predicate)
                    .or(SpecialCharClassifier.predicate)
                    .test(entry.getValue())
            )
            .forEachOrdered(entry -> {
                int i_start = entry.getKey();
                int i_end = i_start + entry.getValue().length() + 1;
                boolean tt = chars[i_start] == t_start;

                chars[i_start] = delim.apply(tt ? "t1_start" : "t2_start");
                chars[i_end] = delim.apply(tt ? "t1_end" : "t2_end");
            });

        return new String(chars);
    }

} // BugReportPhase1Transform
