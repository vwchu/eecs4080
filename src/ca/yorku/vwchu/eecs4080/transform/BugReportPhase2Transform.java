/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.transform;
import ca.yorku.vwchu.eecs4080.structs.*;
import java.util.*;
import java.util.function.*;
import static ca.yorku.vwchu.eecs4080.util.CollectionUtil.*;

/**
 * The phase 2 transformation for the entire bug
 * report, specifies the fields that need to be
 * transformed: phase, description, summary and
 * comments.content fields.
 *
 * Transforms the bundle, at phase 1 to phase 2,
 * converts the comments, description and summary
 * markup text into islands of technical and natural
 * language.
 */
public class BugReportPhase2Transform {

    /**
     * Transforms the given bundle that represents
     * an entire bug report, at phase 1 to phase 2,
     * converts the comments, description and summary
     * markup text into islands of technical and natural
     * language.
     *
     * @param bundle
     *      bundle that represents an entire bug report
     * @return
     *      the given bundle transformed
     */
    public static Bundle transform(Bundle bundle) {
        return BaseBugReportTransform.<Bundle, Bundle>transform(bundle, 2, b ->
            b.assign("stats", BugReportPhase2Transform.computeStats(b))
             .transform(bundle, BugReportPhase2Transform::computeGlobalStats)
        );
    }

    /// MARK: - Private Helpers

    /**
     * Computes the statistic values of the given
     * bundle for phase 2 transformation. Statistics
     * include: number of words, number of technical words,
     * percent of technical words and number of technical
     * phrases computed.
     *
     * @param bundle
     *      to analyze for statistics
     * @return
     *      stats bundle
     */
    private static Bundle computeStats(Bundle bundle) {
        TextNode tb = makeTextNodeTree(bundle);
        return (new Bundle())
            .assign("w_count", tb.w_count)
            .assign("tw_count", tb.tw_count)
            .assign("tw_percent", String.format("%.3f%%", tb.tw_percent * 100))
            .assign("tw_phrases", tb.tw_phrases);
    }

    /**
     * Computes the global statistic values with the given
     * bundle for phase 2 transformation and the global bundle.
     *
     * Statistics include: number of words, number of technical
     * words, percent of technical words and number of technical
     * phrases computed.
     *
     * @param bundle
     *      local bundle with statistics
     * @param global
     *      global shared bundle with
     *      aggregated statistics
     */
    private static void computeGlobalStats(Bundle bundle, Bundle global) {
        Bundle bStats = bundle.value("stats");
        Bundle gStats = global.value("stats");

        if (gStats == null) {
            global.assign("stats", gStats = (new Bundle())
                .assign("w_count", 0)
                .assign("tw_count", 0)
                .assign("tw_percent", 0.0)
                .assign("tw_phrases", 0)
                .assign("tw_comments", 0)
            );
        }

        Function<String, Function<Integer, Integer>> summation = key ->
            sum -> sum + (int)bStats.value(key);
        Function<Integer, Integer> countTWC =
            value -> value + ((int)bStats.value("tw_phrases") > 0 ? 1 : 0);
        Function<Object[], Double> percent = values ->
            ((int)values[1] == 0) ? 0 :
            ((double)((int)values[0]) / (int)values[1]) * 100;

        gStats.reassign("w_count", summation.apply("w_count"))
            .reassign("tw_count", summation.apply("tw_count"))
            .reassign("tw_phrases", summation.apply("tw_phrases"))
            .reassign("tw_comments", countTWC)
            .manipulate("tw_percent", "tw_count w_count".split(" "), percent);
    }

    /**
     * Constructs a TextNode tree data structure,
     * given bug report comment's bundle. Computes the
     * nodal stats, for each text region. Bottom-up.
     *
     * @param bundle
     *      comment bundle of bug report
     * @return
     *      root text node of newly constructed tree
     */
    private static TextNode makeTextNodeTree(Bundle bundle) {
        String annotated = bundle.value("annotated");
        Map<Integer, String> words = bundle.value("words");
        Function<String, Character> delim = bundle.value("delimFn");
        char s_tag = delim.apply("start_tag");
        char e_tag = delim.apply("end_tag");
        Bundle temp = (new Bundle())
            .assign("root", new TextNode(0))
            .assign("current", "root", tb -> tb);

        forEachWithIndex(annotated.chars().mapToObj(i -> (char) i), (i, c) -> {
            TextNode block = temp.value("current");
            if (c == s_tag) {
                if (i > 0) {
                    if (block.nodes.isEmpty()) {
                        block.add(new TextNode(bundle, block.start, i));
                    } else if (block.last().end != i - 1) {
                        block.add(new TextNode(bundle, block.last().end, i));
                    }
                }
                temp.assign("current", block.add(new TextNode(i)).last());
            } else if (c == e_tag) {
                if (!block.nodes.isEmpty() && block.last().end != i - 1) {
                    block.add(new TextNode(bundle, block.last().end, i));
                }
                block.setEnd(bundle, i);
                temp.assign("current", block.parent);
            }
        });

        TextNode tblk = temp.value("root");
        tblk.setEnd(bundle, annotated.length() - 1);
        return tblk;
    }

} // BugReportPhase2Transform

class TextNode {

    int start, end;
    List<TextNode> nodes = new LinkedList<>();
    TextNode parent = null;
    TextNode next = null;
    TextNode prev = null;
    int w_count = 0;
    int tw_count = 0;
    int tw_phrases = 0;
    double tw_percent = 0;

    /**
     * Constructs a new text node given
     * start index in text.
     *
     * @param start
     *      start index of text region
     */
    public TextNode(int start) {
        this.start = start;
    }

    /**
     * Constructs a new text node given
     * bundle data, and start and end index in
     * text.
     *
     * @param bundle
     *      content data
     * @param start
     *      start index of text region
     * @param end
     *      end index of text region
     */
    public TextNode(Bundle bundle, int start, int end) {
        this(start);
        this.setEnd(bundle, end);
    }

    /**
     * Set text node with given bundle data
     * and end index in text.
     *
     * @param bundle
     *      content data
     * @param end
     *      end index of text region
     */
    public void setEnd(Bundle bundle, int end) {
        final String annotated = bundle.value("annotated");
        final Map<Integer, String> words = bundle.value("words");
        final Function<String, Character> delim = bundle.value("delimFn");
        final char t_start = delim.apply("t_start");
        final char t1_start = delim.apply("t1_start");
        final char t2_start = delim.apply("t2_start");

        this.end = end;
        if (nodes.isEmpty()) {
            for (int i = start; i < end; i++) {
                if (words.containsKey(i)) {
                    char ch = annotated.charAt(i);
                    if (ch == t_start || ch == t1_start || ch == t2_start) {
                        /*System.out.printf("%-5d %s\n", i, annotated.substring(
                                i + 1, i + words.get(i).length() + 1));*/
                        tw_count++;
                    }
                    w_count++;
                }
            }
        } else {
            for (TextNode tb : nodes) {
                w_count += tb.w_count;
                tw_count += tb.tw_count;
                tw_phrases += tb.tw_phrases; // FIXME
            }
        }
        tw_percent = w_count > 0 ? ((double)tw_count)/w_count : 0;
        if (tw_phrases == 0) {
            tw_phrases += (w_count > 10 && tw_percent >= 0.3) ? 1 : 0;
        }
        //System.out.printf("%d %d %f %d\n", w_count, tw_count, tw_percent, tw_phrases);
        //System.out.println(annotated.substring(start, end));
    }

    /**
     * Adds the given node as a children to
     * this text node.
     *
     * @param node
     *      children node to add
     * @return
     *      this text node object
     */
    public TextNode add(TextNode node) {
        nodes.add(node);
        node.parent = this;
        node.prev = last();
        node.prev.next = node;
        return this;
    }

    /**
     * Returns the first children TextNode.
     *
     * @return
     *      first children or null
     */
    public TextNode first() {
        return nodes.isEmpty() ? null : nodes.get(0);
    }

    /**
     * Returns the last children TextNode.
     *
     * @return
     *      last children or null
     */
    public TextNode last() {
        return nodes.isEmpty() ? null : nodes.get(nodes.size() - 1);
    }

} // TextNode
