/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.transform;
import ca.yorku.vwchu.eecs4080.structs.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * The base transformation for the entire bug
 * report, specifies the fields that need to be
 * transformed: phase, description, summary and
 * comments.content fields.
 */
public class BaseBugReportTransform {

    /**
     * Transforms the given bundle that represents
     * an entire bug report, at the given phase,
     * and with the given transform function for
     * specifies the fields that need to be
     * transformed: phase, description, summary and
     * comments.content fields.
     *
     * @param bundle
     *      bundle that represents an entire bug report
     * @param phase
     *      phase of the transformation
     * @param transform
     *      transform function for specifies the fields
     *      that need to be transformed
     * @return
     *      the given bundle transformed
     */
    public static <T, R> Bundle transform(Bundle bundle, int phase, Function<T, R> transform) {
        return bundle
            .assign("phase", phase)
            .reassign("description", transform)
            .<List<Bundle>, List<Bundle>>reassign("comments", list -> list.stream()
                .map(b -> b.reassign("content", transform))
                .collect(Collectors.toList()));
    }

} // BaseBugReportTransform
