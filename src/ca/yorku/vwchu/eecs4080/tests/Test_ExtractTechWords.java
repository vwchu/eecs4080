/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.tests;
import ca.yorku.vwchu.eecs4080.loader.*;
import ca.yorku.vwchu.eecs4080.structs.*;
import ca.yorku.vwchu.eecs4080.transform.*;
import java.io.*;
import java.util.*;
import java.util.function.*;

public class Test_ExtractTechWords {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            args = new String[] {"tests/supervised/bugreport.xml"};
        }
        BugReportRawLoader
            .load(args[0])
            .transform(BugReportPhase1Transform::transform)
            .transform(Test_ExtractTechWords::transform);
    }
    public static Bundle transform(Bundle b) {
        Set<String> wordSet = new HashSet<>();
        b = BaseBugReportTransform.<Bundle, Bundle>transform(b, 3, bundle -> {
            Function<String, Character> delim = bundle.value("delimFn");
            Map<Integer, String> words = bundle.value("words");
            String text = bundle.value("annotated");

            words.entrySet().stream()
                .filter(e -> text.charAt(e.getKey()) == delim.apply("t_start"))
                .map(Map.Entry::getValue)
                .distinct()
                .forEach(wordSet::add);
            return bundle;
        });
        wordSet.stream().forEach(System.out::println);
        return b;
    }
}
