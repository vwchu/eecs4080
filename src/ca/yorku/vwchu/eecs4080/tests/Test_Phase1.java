/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.tests;
import ca.yorku.vwchu.eecs4080.loader.*;
import ca.yorku.vwchu.eecs4080.storer.*;
import ca.yorku.vwchu.eecs4080.transform.*;
import java.io.*;

public class Test_Phase1 {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            args = new String[] {"tests/supervised/bugreport.xml"};
        }
        BugReportRawLoader
            .load(args[0])
            .transform(BugReportPhase1Transform::transform)
            .store(args[0].replaceAll("[.]xml$", ".1.xml"),
                BugReportPhase1Storer::store);
    }
}
