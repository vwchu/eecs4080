/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.tests;
import ca.yorku.vwchu.eecs4080.loader.*;
import ca.yorku.vwchu.eecs4080.storer.*;
import ca.yorku.vwchu.eecs4080.structs.*;
import ca.yorku.vwchu.eecs4080.transform.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class Test_Sample {
    public static void main(String[] args) throws IOException {
        File root = new File("tests/samples/");
        File outDir = new File(root, "output/");

        // Prepare the output directory

        if ((outDir.exists() && !outDir.isDirectory()) || !outDir.mkdir()) {
            throw new IOException("cannot make " + outDir.getAbsolutePath() + " directory");
        } else {
            // clean up directory content
            Stream.of(outDir.listFiles(
                (d, n) -> n.endsWith(".xml") ||
                          n.endsWith(".csv"))).forEach(File::delete);
        }

        try (PrintWriter out = new PrintWriter(new File(root, "output/output.csv"));
             PrintWriter o2 = new PrintWriter(new File(root, "output/fine-output.csv"))) {

            // Coarse Output Headings
            String headings = "id,link,w_count,tw_count,tw_phrases,tw_comments,tw_percent";
            System.out.println("input,output," + headings);
            out.println(headings);
            out.flush();

            // Fine-grain Output Headings
            String fHeadings = "id,link,block_id,w_count,tw_count,tw_percent,tw_phrases,is_technical";
            o2.println(fHeadings);
            o2.flush();

            Stream.of(root.listFiles((d, n) -> n.endsWith(".xml"))).forEach(file -> {
                try {
                    String id = file.getName().replace(".xml", "");
                    String inFile = file.getAbsolutePath();
                    String outFile = (new File(file.getParentFile(), "output/" +
                        file.getName())).getAbsolutePath();

                    // Run the transformations and write generated file

                    Bundle bundle = BugReportRawLoader.load(inFile)
                        .transform(BugReportPhase1Transform::transform)
                        .transform(BugReportPhase2Transform::transform)
                        .transform(b -> ((Bundle)b.value("stats"))
                            .<Double, String>reassign("tw_percent",
                                p -> String.format("%.3f%%", p)))
                        .store(outFile, BugReportPhase2Storer::store);

                    String link = bundle.value("link"); // Bug report URL

                    // Write Coarse Output

                    Bundle stats = bundle.value("stats");
                    String record = String.format("%s,%s,%d,%d,%d,%d,%s", id, link,
                        (int) stats.value("w_count"),
                        (int) stats.value("tw_count"),
                        (int) stats.value("tw_phrases"),
                        (int) stats.value("tw_comments"),
                        (String) stats.value("tw_percent"));

                    System.out.printf("%s,%s,%s\n", inFile, outFile, record);
                    out.println(record);
                    out.flush();

                    // Write Fine-grained Output

                    BiConsumer<String, Bundle> writeFineRecords = (cid, cstats) ->
                        o2.printf("%s,%s,%s,%d,%d,%s,%d,%d\n", id, link, cid,
                            (int) cstats.value("w_count"),
                            (int) cstats.value("tw_count"),
                            (String) cstats.value("tw_percent"),
                            (int) cstats.value("tw_phrases"),
                            ((int) cstats.value("tw_phrases")) > 0 ? 1 : 0);

                    Map<String, Bundle> textBlocks = new LinkedHashMap<>();
                    textBlocks.put("desc", bundle
                        .<Bundle>value("description").value("stats"));
                    bundle.<List<Bundle>>value("comments").stream()
                        .forEachOrdered(b -> textBlocks.put(
                            b.<String>value("id"),
                            b.<Bundle>value("content").value("stats")));

                    textBlocks.entrySet().stream().forEachOrdered(e ->
                        writeFineRecords.accept(e.getKey(), e.getValue()));
                    o2.flush();

                } catch (Exception e) {
                    e.printStackTrace(System.err);
                }
            });
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }
} // Test_Sample
