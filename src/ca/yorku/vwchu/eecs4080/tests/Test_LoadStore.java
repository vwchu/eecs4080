/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.tests;
import ca.yorku.vwchu.eecs4080.loader.*;
import ca.yorku.vwchu.eecs4080.storer.*;
import java.io.*;

public class Test_LoadStore {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            args = new String[] {"tests/supervised/bugreport.xml"};
        }
        BugReportRawLoader
            .load(args[0])
            .store(args[0].replaceAll("[.]xml$", ".0.xml"),
                BugReportPhase1Storer::store);
    }
}
