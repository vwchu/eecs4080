/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.tests;
import ca.yorku.vwchu.eecs4080.loader.*;
import ca.yorku.vwchu.eecs4080.storer.*;
import ca.yorku.vwchu.eecs4080.transform.*;
import java.io.*;

public class Test_Phase2 {
    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            args = new String[] {"tests/supervised/bugreport.xml"};
        }
        BugReportRawLoader
            .load(args[0])
            .transform(BugReportPhase1Transform::transform)
            .transform(BugReportPhase2Transform::transform)
            .transform(b -> b.<Double, String>reassign("tw_percent", p -> String.format("%.3f%%", p)))
            .store(args[0].replaceAll("[.]xml$", ".2.xml"), BugReportPhase2Storer::store);
    }
}
