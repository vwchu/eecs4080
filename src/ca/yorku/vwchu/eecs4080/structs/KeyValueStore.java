/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.structs;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * The KeyValueStore class representing a
 * logical data component and a container for
 * carrying various pieces of information
 * and metadata.
 */
public class KeyValueStore<K, V> extends LinkedHashMap<K, V> {

    /**
     * Adds the given map to this map.
     * Copies the key-value entries from the given
     * map to this map.
     *
     * @param map
     *      key-value entries to be added
     * @return
     *      this map object
     */
    public KeyValueStore<K, V> add(KeyValueStore<K, V> map) {
        map.forEach(this::put);
        return this;
    }

    // MARK: - Getters

    /**
     * Returns the value of the given
     * property field key name.
     *
     * @param key
     *      name of property field to be retrieved
     * @return
     *      value of the property field
     */
    @SuppressWarnings("unchecked")
    public <T extends V> T value(K key) {
        return (T)get(key);
    }

    /**
     * Returns the value of the given
     * property field key name with the given
     * transformation performed.
     *
     * @param key
     *      name of property field to be retrieved
     * @param transform
     *      function to alter the returned value
     * @return
     *      value of the property field with
     *      transformation applied.
     */
    @SuppressWarnings("unchecked")
    public <T extends V, R> R value(K key, Function<T, R> transform) {
        return transform.apply(value(key));
    }

    // MARK: - Setters

    /**
     * Assigns the given key with the given value.
     *
     * @param key
     *      name of property field to be set
     * @param value
     *      value to be set as the property field
     * @return
     *      this map object
     */
    public KeyValueStore<K, V> assign(K key, V value) {
        put(key, value); return this;
    }

    /**
     * Assigns the given array of keys with the evaluated
     * results from the expression for each key.
     *
     * @param keys
     *      array of keys
     * @param expr
     *      expression to evaluate to obtain value for each key
     * @return
     *      this map object
     */
    public KeyValueStore<K, V> assign(K[] keys, Function<K, V> expr) {
        Stream.of(keys).forEach(key -> assign(key, expr.apply(key)));
        return this;
    }

    /**
     * Assigns the given key with the evaluated results
     * from the expression with the value from the given
     * key as input.
     *
     * @param key
     *      name of property field to be set
     * @param valueKey
     *      name of property field where expression input stored
     * @param expr
     *      expression to evaluate to obtain value for each key
     * @return
     *      this map object
     */
    public <T extends V, R extends V> KeyValueStore<K, V> assign(
            K key, K valueKey, Function<T, R> expr) {
        return assign(key, value(valueKey, expr));
    }

    /**
     * Replaces or transforms the value stored
     * at the given key to a new value, generated
     * given the transform function that takes
     * the old value and outputs the new one based
     * on it.
     *
     * @param key
     *      name of property field to be set
     * @param transform
     *      function to alter the stored value
     * @return
     *      this map object
     */
    @SuppressWarnings("unchecked")
    public <T extends V, R extends V> KeyValueStore<K, V> reassign(
            K key, Function<T, R> transform) {
        return assign(key, value(key, transform));
    }

    /**
     * Returns a filtered map given array keys
     * to include in the output map.
     *
     * @param keys
     *      array of keys to include
     * @return
     *      map with only filtered keys
     */
    public KeyValueStore<K, V> filter(K[] keys) {
        KeyValueStore<K, V> map = new KeyValueStore<>();
        List<K> keysList = Arrays.asList(keys);
        if (keysList.isEmpty()) {
            return map.add(this);
        }
        entrySet().stream()
            .filter(entry -> keysList.contains(entry.getKey()))
            .forEach(entry -> put(entry.getKey(), entry.getValue()));
        return map;
    }

} // KeyValueStore
