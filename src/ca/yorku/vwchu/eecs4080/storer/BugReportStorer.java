/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.storer;
import ca.yorku.vwchu.eecs4080.structs.*;
import ca.yorku.vwchu.eecs4080.util.*;
import org.joox.*;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import static org.joox.JOOX.*;

/**
 * Storer for bug report data structure.
 * Writes and serializes a Bundle representing
 * a bug report to the XML bug report file.
 */
public class BugReportStorer {

    /**
     * Stores and serializes the given bundle into
     * the file corresponding to the given filename
     * and output map.
     *
     * @param bundle
     *      the bug report object to serialize
     * @param filename
     *      the file path to write to
     * @param outputMap
     *      the map that describes the output fields and attributes
     */
    public static void store(Bundle bundle, String filename, Map<String, String> outputMap) {
        XMLUtil.serialize(makeNode("bugreport", bundle, null, outputMap), new File(filename));
    }

    /// MARK: - Public Helpers

    /**
     * Given a set of strings, concats them
     * all together separated by space.
     *
     * @param strs
     *      string segments to join
     * @return
     *      the joined string
     */
    public static String join(String... strs) {
        return String.join(" ", strs);
    }

    /**
     * Given a behaviour rule name, a set of space-delimited
     * field strings, expands the fields into fully qualified fields
     * (dot-qualified names) (cross-product) and appends the rule
     * at the end of each fully qualified field name, delimited
     * by colon.
     *
     *  Rules:
     *      attr    - output as an element attribute
     *      content - output as the text content in the element
     *      exclude - field to exclude in the output
     *
     *  Example:
     *      Given: ("rule", "a b.bb c", "d e f", "g h")
     *      Output:
     *          a.d.g:rule a.e.g:rule a.f.g:rule
     *          a.d.h:rule a.e.h:rule a.f.h:rule
     *          b.bb.d.g:rule b.bb.e.g:rule b.bb.f.g:rule
     *          b.bb.d.h:rule b.bb.e.h:rule b.bb.f.h:rule
     *          c.d.g:rule c.e.g:rule c.f.g:rule
     *          c.d.h:rule c.e.h:rule c.f.h:rule
     *
     * @param rule
     *      behaviour rule name, one of "attr",
     *      "content", or "exclude"
     * @param fields
     *      array of space-delimited field
     *      strings for each level
     * @return
     *      space-delimited list of expansion
     */
    public static String rules(String rule, String ...fields) {
        if (fields.length == 0) {return "";}
        List<String> fieldList = Arrays.asList(fields[0].split(" "));
        for (int i = 1; i < fields.length; i++) {
            String[] subs = fields[i].split(" ");
            fieldList = fieldList.stream()
                .flatMap(name -> Stream.of(subs).map(sub -> name + "." + sub))
                .collect(Collectors.toList());
        }
        return fieldList.stream()
            .map(name -> name + ":" + rule)
            .collect(Collectors.joining(" "));
    }

    // MARK: - Private Helpers

    /**
     * Creates a new XML match tree node for the
     * given bundle to serialize.
     *
     * @param tagName
     *      tag name of the element
     * @param bundle
     *      bundle to serialize
     * @param inherited
     *      the map that describes the inherited output fields and attributes
     * @param outputMap
     *      the map that describes the output fields and attributes
     * @return
     *      XML node (JOOX match)
     */
    private static Match makeNode(String tagName, Bundle bundle,
        Map<String, String> inherited,
        Map<String, String> outputMap)
    {
        Match root = $(tagName);

        // Parse the given outputMap a map of elements (key) to a
        // space delimited list of attributes and types into a map of
        // attribute names (keys) to interpretation types (values),
        // including inherited pairs.
        Map<String, String> properties = inherited != null ? inherited : new HashMap<>();
        Stream.of((outputMap.containsKey(tagName) ? outputMap.get(tagName) : "").split(" "))
            .map(tag -> tag.split(":"))
            .forEachOrdered(parts -> properties.put(parts[0], parts.length > 1 ? parts[1] : ""));

        // Build a map of map of nested attributes to types from the output map.
        Map<String, Map<String, String>> passedProps = new HashMap<>();
        properties.keySet().stream().filter(key -> key.contains(".")).forEachOrdered(key -> {
            String[] pair = key.split("[.]", 2);
            if (!passedProps.containsKey(pair[0])) {
                passedProps.put(pair[0], new HashMap<>());
            }
            passedProps.get(pair[0]).put(pair[1], properties.get(key));
        });

        // For each field in the bundle
        bundle.forEach((k, v) -> {
            if (properties.containsKey(k) && "exclude".equals(properties.get(k))) {return;}
            if (v instanceof Bundle) {
                root.append(makeNode(k, (Bundle) v, passedProps.get(k), outputMap));
            } else if (v instanceof Match) {
                root.append($(k, (Match) v));
            } else if (v instanceof List) {
                Match listRoot = $(k);
                String singular = k.substring(0, k.length() - 1);
                Stream<Match> matches;
                try {
                    List<Bundle> bundles = bundle.value(k);
                    matches = bundles.stream().map(
                        m -> makeNode(singular, m, passedProps.get(k), outputMap));
                } catch (Exception e) {
                    List<Object> list = bundle.value(k);
                    matches = list.stream().map(
                        o -> $(singular).text(o == null ? "" : o.toString()));
                }
                matches.forEachOrdered(listRoot::append);
                root.append(listRoot);
            } else {
                if (v instanceof Date) {
                    v = bundle.<Date, String>value(k, DateUtil::datetime);
                }
                if (properties.containsKey(k)) {
                    switch (properties.get(k)) {
                        case "attr":
                            root.attr(k, v == null ? "" : v.toString());
                            break;
                        case "content":
                            root.text(v == null ? "" : v.toString());
                            break;
                        default:
                            root.append($(k).text(v == null ? "" : v.toString()));
                    }
                } else {
                    root.append($(k).text(v == null ? "" : v.toString()));
                }
            }
        });
        return root;
    }

} // BugReportStorer
