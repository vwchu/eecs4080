/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.storer;
import ca.yorku.vwchu.eecs4080.structs.*;
import static ca.yorku.vwchu.eecs4080.storer.BugReportStorer.*;

/**
 * Storer for bug report data structure.
 * Writes and serializes a Bundle representing
 * a bug report to the XML bug report file
 * after phase 1 manipulations and alterations.
 */
public class BugReportPhase1Storer {

    /**
     * Stores and serializes the given bundle into
     * the file corresponding to the given filename.
     *
     * @param bundle
     *      the bug report object to serialize
     * @param filename
     *      the file path to write to
     */
    public static void store(Bundle bundle, String filename) {
        BugReportStorer.store(bundle, filename, (new KeyValueStore<String, String>())
            .assign("bugreport", join(
                rules("attr", "phase key project"),
                rules("exclude", "description", "text xmldom delimFn words")))
            .assign("comment", join(
                rules("attr", "id author created"),
                rules("exclude", "content", "text xmldom delimFn words")))
            .assign("attachment",
                rules("attr", "id name size author created"))
            .assign("subtask", join(
                rules("attr", "id"),
                rules("content", "content")))
            .assign("issue", join(
                rules("attr", "id type link_type"),
                rules("content", "content")))
        );
    }

} // BugReportPhase1Storer
