/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.helpers;
import java.io.*;
import java.nio.channels.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;

public class Copy_SampleFiles {

    private static Map<String, String> fileMap = new HashMap<>();
    private static Map<String, String> sampleList = new HashMap<>();

    public static void main(String[] args) {
        if (args.length < 2) return;
        loadFileMap(args[0]);
        loadSampleList(args[1]);
        sampleList.entrySet().parallelStream()
            .filter(e -> fileMap.containsKey(e.getValue()))
            .forEach(e ->
                copyFile(new File(fileMap.get(e.getValue())),
                         new File("tests/samples/" + e.getKey() + ".xml")));
    }

    public static void copyFile(File sourceFile, File destFile) {
        try {
            if (!destFile.exists() && !destFile.createNewFile()) {return;}
            try (FileChannel source = new FileInputStream(sourceFile).getChannel();
                 FileChannel destination = new FileOutputStream(destFile).getChannel()) {
                System.out.printf("Copying %s -> %s\n", sourceFile, destFile);
                destination.transferFrom(source, 0, source.size());
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    private static void loadFileMap(String fileMapFilename) {
        try {
            Files.lines(FileSystems.getDefault().getPath(fileMapFilename),
                        StandardCharsets.UTF_8)
                .map(l -> l.split(","))
                .filter(f -> f.length >= 2)
                .forEach(f -> fileMap.put(f[0], f[1]));
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    private static void loadSampleList(String sampleListFilename) {
        try {
            Files.lines(FileSystems.getDefault().getPath(sampleListFilename),
                        StandardCharsets.UTF_8)
                .map(l -> l.split(","))
                .filter(f -> f.length >= 2)
                .forEachOrdered(f -> sampleList.put(f[0], f[1]));
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

} // Copy_SampleFiles
