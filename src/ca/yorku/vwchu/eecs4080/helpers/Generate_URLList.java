/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.helpers;
import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.stream.*;
import static org.joox.JOOX.*;

/**
 * Generates the list of the bug report issue URLs.
 * Creates a file with each filepath and url pair
 * delimited by a comma on a line. The URLs are
 * pulled from bug report URL files in the dataset.
 *
 * Usage: java Generate_URLList DIRPATH OUTFILE
 *
 *      DIRPATH     Root base directory path containing all bug reports
 *      OUTFILE     File path to output file to write to
 */
public class Generate_URLList {

    public static void main(String[] args) {
        if (args.length < 2) return;
        try (PrintWriter out = new PrintWriter(args[1])) {
            streamXMLFiles(args[0]).parallel()
                .map(File::getAbsolutePath)
                .map(m -> String.format("%s,%s", getBugReportLink(m), m))
                .filter(m -> m != null)
                .peek(System.out::println)
                .forEach(out::println);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * Returns the stream of a list of XML files
     * in the given file path for the directory
     * containing the bug reports.
     *
     * @param path
     *      directory containing the bug reports
     * @return
     *      stream of bug report files
     */
    private static Stream<File> streamXMLFiles(String path) {
        File root = new File(path);
        return Stream.concat(
            Stream.of(root.listFiles((d, n) -> n.endsWith(".xml"))),
            Stream.of(root.listFiles(File::isDirectory)).parallel()
                .flatMap(dir -> streamXMLFiles(dir.getAbsolutePath())));
    }

    /**
     * Returns the corresponding XML bug report's
     * online URL link of the given filename.
     *
     * @param filename
     *      file path of the XML bug report
     * @return
     *      bug report URL link
     */
    private static String getBugReportLink(String filename) {
        try {
            return $("<?xml version='1.0' encoding='utf-8'?>" +
                Files.lines(FileSystems.getDefault().getPath(filename),
                            StandardCharsets.UTF_8)
                    .collect(Collectors.joining(System.lineSeparator()))
                    .replaceAll("[&]#8220;", "\"")
                    .replaceAll("[&]#8221;", "\"")
                    .replaceAll("[&]#8217;", "\'")
            ).find("rss > channel > item > link").text();
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }
    }

} // Generate_URLList
