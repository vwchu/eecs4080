/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.helpers;
import ca.yorku.vwchu.eecs4080.util.XMLUtil;
import org.joox.*;
import java.io.*;
import java.util.*;
import java.util.stream.*;
import static org.joox.JOOX.*;

/**
 * Generates the list of programming language keywords
 * for the Phase 1 Pass 2. Creates a file with each
 * keyword on a line. The keywords are pulled from
 * language specification files from the GNOME
 * gtksourceview syntax highlighting project.
 */
public class Generate_KeywordList {

    public static void main(String[] args) {
        String basePath = System.getProperty("user.dir") + "/assets/dicts/keywords";
        File f = new File(basePath, "gtksourceview/language-specs");
        Match root = $("languages");

        // Process each .lang file in parallel, for each <keyword>
        // tag, take the text contain and add it to the keywords set data
        // structure (synchronized) if does not exist already.
        Stream.of(f.listFiles((d, n) -> n.endsWith(".lang")))
            .parallel().map(file -> {
                try {
                    return $(file);
                } catch (Exception e) {
                    e.printStackTrace(System.err); return null;
                }
            })
            .filter(m -> m != null)
            .forEach(m -> {
                Set<String> keywords = new HashSet<>();
                Match langRoot = $("language")
                    .attr("id", m.attr("_name"))
                    .attr("mimetypes", m.find("metadata > property[name=mimetypes]").text())
                    .attr("globs", m.find("metadata > property[name=globs]").text());

                m.find("keyword").texts().forEach(word -> {
                    if (!keywords.contains(word)) {
                        keywords.add(word);
                        langRoot.append($("keyword").text(word));
                    }
                });
                root.append(langRoot);
            });

        XMLUtil.serialize(root, new File(basePath, "keywords.xml"));
    }

} // Generate_KeywordList
