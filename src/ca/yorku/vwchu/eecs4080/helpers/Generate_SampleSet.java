/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.helpers;
import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.*;
import static ca.yorku.vwchu.eecs4080.util.CollectionUtil.*;

public class Generate_SampleSet {

    public static void main(String[] args) {
        if (args.length < 3) return;
        try (PrintWriter out = new PrintWriter(args[2])) {
            List<String> files = Files.lines(
                FileSystems.getDefault().getPath(args[0]),
                StandardCharsets.UTF_8).collect(Collectors.toList());
            Collections.shuffle(files);
            mapWithIndex(files.subList(0, Integer.parseInt(args[1])).stream(),
                    (i, e) -> String.format("%03d,%s", i + 1, e))
                .peek(e -> System.out.println(e.replaceFirst(",", " ")))
                .forEach(out::println);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

} // Generate_SampleSet
