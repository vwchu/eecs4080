/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.util;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

/**
 * Collection of static methods to
 * manipulate and handle collections
 * and streams.
 */
public class CollectionUtil {
    private CollectionUtil() { }

    /**
     * Converts an {@link java.util.Iterator} to
     * {@link java.util.stream.Stream}.
     *
     * @param iterator
     *      the iterator to convert to stream
     * @return
     *      the stream for the iterator
     */
    public static <T> Stream<T> iterate(Iterator<? extends T> iterator) {
        int characteristics = Spliterator.ORDERED | Spliterator.IMMUTABLE;
        return StreamSupport.stream(
            Spliterators.spliteratorUnknownSize(iterator, characteristics),
            false);
    }

    /**
     * Zips the specified stream with its indices.
     *
     * @param stream
     *      the stream to zip with its indices
     * @return
     *      the stream with indices
     */
    public static <T> Stream<Map.Entry<Integer, T>> zipWithIndex(Stream<? extends T> stream) {
        return iterate(new Iterator<Map.Entry<Integer, T>>() {
            private final Iterator<? extends T> streamIterator = stream.iterator();
            private int index = 0;

            @Override
            public boolean hasNext() {
                return streamIterator.hasNext();
            }

            @Override
            public Map.Entry<Integer, T> next() {
                return new AbstractMap.SimpleImmutableEntry<>(index++,
                    streamIterator.next());
            }
        });
    }

    /**
     * Returns a stream consisting of the results of applying the
     * given two-arguments function to the elements of this stream.
     * The first argument of the function is the element index and
     * the second one - the element value.
     *
     * @param stream
     *      stream of elements
     * @param mapper
     *      function to get index and value to
     *      generate new stream of values
     * @return
     *      stream of generated values
     */
    public static <T, R> Stream<R> mapWithIndex(
            Stream<? extends T> stream,
            BiFunction<Integer, ? super T, ? extends R> mapper) {
        return zipWithIndex(stream).map(entry ->
            mapper.apply(entry.getKey(),
                         entry.getValue()));
    }

    /**
     * Given two-arguments function to the elements of this stream.
     * The first argument of the function is the element index and
     * the second one - the element value, iterates the given
     * stream of element.
     *
     * @param stream
     *      stream of elements
     * @param func
     *      function to get index and value to
     *      generate new stream of values
     */
    public static <T> void forEachWithIndex(
            Stream<? extends T> stream,
            BiConsumer<Integer, ? super T> func) {
        zipWithIndex(stream).forEachOrdered(entry ->
            func.accept(entry.getKey(), entry.getValue()));
    }

} // CollectionUtil
