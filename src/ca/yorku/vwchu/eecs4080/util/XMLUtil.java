/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.util;
import org.joox.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.*;

/**
 * Collection of static methods to
 * manipulate and handle XML and JOOX
 * for serialization.
 */
public class XMLUtil {

    /**
     * Serializes the given XML match
     * object into the given file.
     *
     * @param match
     *      the XML object to serialize
     * @param file
     *      the file to write to
     */
    public static void serialize(Match match, File file) {
        try (PrintWriter out = new PrintWriter(file)) {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            Source source = new DOMSource(match.document());
            Result target = new StreamResult(buffer);

            Transformer trans = TransformerFactory.newInstance().newTransformer();
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            trans.transform(source, target);

            out.println("<?xml version='1.0' encoding='utf-8'?>");
            out.println();
            out.println(buffer.toString());
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

} // XMLUtil
