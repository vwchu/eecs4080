/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.util;
import java.text.*;
import java.util.*;

/**
 * Static class that handles representation,
 * parsing and serialization of a date object
 * in the bundle objects.
 */
public class DateUtil {

    static final DateFormat DATE_FORMAT =
            new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");

    /**
     * Parse the given string as an datetime.
     *
     * @param dateString
     *      the string that represents a datetime
     * @return
     *      the datetime value or null if error occurred
     */
    public static Date datetime(String dateString) {
        try {
            return DATE_FORMAT.parse(dateString);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Convert the given datetime to a string.
     *
     * @param datetime
     *      the date time object
     * @return
     *      the string that represents a datetime
     */
    public static String datetime(Date datetime) {
        return DATE_FORMAT.format(datetime);
    }

} // DateUtil
