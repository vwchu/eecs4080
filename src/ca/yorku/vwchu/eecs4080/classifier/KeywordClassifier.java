/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.classifier;
import ca.yorku.vwchu.eecs4080.structs.*;
import org.joox.*;
import java.nio.file.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static org.joox.JOOX.*;

/**
 * Classsifier defines how the text are
 * categorized as either technical or non-technical,
 * using the list of programming language keywords.
 */
public class KeywordClassifier {

    public static Predicate<String> predicate(String text) {
        final Set<String> ss = Stream.of(text.split("[ \"]"))
                .parallel().distinct().collect(Collectors.toSet());

        final Set<String> dict = dictionary.entrySet().parallelStream()
            .filter(e -> e != null)
            .filter(e -> e.getKey() != null && (ss.contains(e.getKey()) ||
                ss.contains("code-" + e.getKey().toLowerCase())))
            //.peek(e -> System.out.println(e.getKey()))
            .<Set<String>>map(e -> e.getValue().value("keywords"))
            .filter(e -> e != null)
            .flatMap(Set::stream).distinct()
            .collect(Collectors.toSet());

        return (word) ->
            dict.contains(word) || dict.parallelStream().anyMatch(regex -> {
                try {
                    return Pattern.matches(regex, word);
                } catch (Exception e) {
                    return false;
                }
            });
    }

    static Map<String, Bundle> dictionary = new HashMap<>();
    static {
        String cwd = System.getProperty("user.dir");
        String dictPath = "assets/dicts/keywords/keywords.xml";
        Path path = Paths.get(cwd, dictPath);
        try {
            $(Files.lines(path).collect(Collectors.joining(System.lineSeparator())))
                .find("language").each().forEach(l -> {
                    String globs = l.attr("globs");
                    String mimes = l.attr("mimetypes");
                    dictionary.put(l.attr("id"), (new Bundle())
                        .assign("globs", Stream.of((globs == null ? "" : globs).split(";"))
                            .map(glob -> glob.replaceAll("\\.", "\\.").replaceAll("\\*", ".*"))
                            .toArray(String[]::new))
                        .assign("mimetypes", (mimes == null ? "" : mimes).split(";"))
                        .assign("keywords", l.children("keyword").each().stream()
                            .map(Match::text).collect(Collectors.toSet())));
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

} // KeywordClassifier
