/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.classifier;
import java.util.function.*;

/**
 * Classsifier defines how the text are
 * categorized as either technical or non-technical,
 * using 4 regular expressions to match
 * a camel case word.
 */
public class SpecialCharClassifier {
    public static final Predicate<String> predicate = SpecialCharClassifier::test;
    public static boolean test(String text) {
        int synbols = 0;
        for (char c : text.toCharArray()) {
            if (!Character.isAlphabetic(c) && !Character.isDigit(c)) {
                synbols++;
            }
        }
        return synbols > 1;
    }
} // SpecialCharClassifier
