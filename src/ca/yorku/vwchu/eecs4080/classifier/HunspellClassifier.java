/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.classifier;
import dk.dren.hunspell.Hunspell.*;
import java.util.function.*;
import static dk.dren.hunspell.Hunspell.*;

/**
 * Classsifier defines how the text are
 * categorized as either technical or non-technical,
 * using the Hunspell spell-checker.
 */
public class HunspellClassifier {

    public static final Predicate<String> predicate = HunspellClassifier::test;
    public static boolean test(String text) {
        return dictionary != null && dictionary.misspelled(text);
    }

    static Dictionary dictionary;
    static {
        try {
            String cwd = System.getProperty("user.dir");
            String dictPath = cwd + "/assets/dicts/hunspell/en_US";
            dictionary = getInstance().getDictionary(dictPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

} // HunspellClassifier
