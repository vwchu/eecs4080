/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.classifier;
import com.swabunga.spell.engine.*;
import com.swabunga.spell.event.*;
import java.io.*;
import java.util.function.*;

/**
 * Classsifier defines how the text are
 * categorized as either technical or non-technical,
 * using the Jazzy spell-checker.
 */
public class JazzyClassifier {

    public static final Predicate<String> predicate = JazzyClassifier::test;
    public static boolean test(String text) {
        return checker != null && !checker.isCorrect(text);
    }

    static SpellChecker checker;
    static {
        try {
            String cwd = System.getProperty("user.dir");
            String dictPath = "assets/dicts/jazzy/english.0";
            File dictFile = new File(cwd, dictPath);
            checker = new SpellChecker();
            checker.addDictionary(new SpellDictionaryHashMap(dictFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

} // JazzyClassifier
