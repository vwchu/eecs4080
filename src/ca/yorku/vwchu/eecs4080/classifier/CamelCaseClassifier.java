/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.classifier;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

/**
 * Classsifier defines how the text are
 * categorized as either technical or non-technical,
 * using 4 regular expressions to match
 * a camel case word.
 */
public class CamelCaseClassifier {

    public static final Predicate<String> predicate = CamelCaseClassifier::test;
    public static boolean test(String text) {
        return text.matches("[A-Za-z0-9_]+") &&
            regex.stream().anyMatch(p -> p.matcher(text).matches());
    }

    static List<Pattern> regex = Stream.of(
        "(\\b([A-Z_][a-z_0-9]*)+[A-Z_0-9][a-z_0-9]+([A-Z_0-9][a-z_0-9]*)*\\b)",
        "(\\b([a-z_][a-z_0-9]*)+([A-Z_0-9][a-z_0-9]*)+\\b)",
        "(\\b([A-Z_][A-Z_0-9]+)\\b)",
        "(\\b(([A-Z_][a-z_0-9]*)+([A-Z_0-9]+))\\b)"
    ).map(Pattern::compile).collect(Collectors.toList());

} // CamelCaseClassifier
