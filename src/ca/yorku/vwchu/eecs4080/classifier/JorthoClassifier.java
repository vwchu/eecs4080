/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
package ca.yorku.vwchu.eecs4080.classifier;
import com.inet.jortho.*;
import java.io.*;
import java.util.function.*;

/**
 * Classsifier defines how the text are
 * categorized as either technical or non-technical,
 * using the Jortho spell-checker.
 */
public class JorthoClassifier {

    public static final Predicate<String> predicate = JorthoClassifier::test;
    public static boolean test(String text) {
        return dictionary != null && !dictionary.exist(text);
    }

    static Dictionary dictionary;
    static {
        try {
            String cwd = System.getProperty("user.dir");
            String dictPath = "assets/dicts/jortho/dictionary_en.ortho";
            DictionaryFactory factory = new DictionaryFactory();
            factory.loadWordList((new File(cwd, dictPath)).toURI().toURL());
            dictionary = factory.create();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

} // JorthoClassifier
