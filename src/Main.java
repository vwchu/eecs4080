/**
 * Copyright 2015 Vincent W. Chu (vwchu@my.yorku.ca)
 * All rights reserved.
 *
 * Component of EECS 4080 project, mining
 * software repository -- extracting software
 * engineering data from bug reports.
 */
import ca.yorku.vwchu.eecs4080.helpers.*;
import ca.yorku.vwchu.eecs4080.tests.*;
import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IOException {
        PrintStream out = System.out;
        if (args.length == 0) {
            out.println(String.join("\n", new String[]{
                "Usage: java Main <COMMAND> <ARGS>\n",
                "  COMMANDS:\n",
                "    test-io        Test load/store of bug reports\n",
                "      infile       Input bug report XML file",
                "      outfile      Output Phase 0 XML file\n",
                "    test-p1        Test Phase 1 transformation of bug reports\n",
                "      infile       Input bug report XML file",
                "      outfile      Output Phase 1 XML file\n",
                "    test-p2        Test Phase 2 transformation of bug reports\n",
                "      infile       Input bug report XML file",
                "      outfile      Output Phase 2 XML file\n",
                "    run-sample     Run algorithm on sample dataset\n",
                "    copy-sample    Copy sample dataset into sample directory\n",
                "      filemap      File with list of file paths to bug reports",
                "      samplelist   File with list of sample dataset bug reports\n",
                "    gen-kwlist     Generate programming language reserved keyword list\n",
                "    gen-sampleset  Generate sample set of given size\n",
                "      filelist     File with list of file paths to bug reports",
                "      size         Number of sample bug reports to be included",
                "      outfile      File to write output result\n",
                "    gen-urllist    Generate list of URL of bug reports\n",
                "      dirpath      Root base directory path containing all bug reports",
                "      outfile      File path to output file to write to\n"
            }));
            System.exit(1);
        }

        String[] argv = args.length == 1 ? new String[]{} :
            Arrays.copyOfRange(args, 1, args.length);

        switch (args[0].toLowerCase()) {
            case "test-io":
                Test_LoadStore.main(argv); break;
            case "test-p1":
                Test_Phase1.main(argv); break;
            case "test-p2":
                Test_Phase2.main(argv); break;
            case "copy-sample":
                Copy_SampleFiles.main(argv); break;
            case "gen-kwlist":
                Generate_KeywordList.main(argv); break;
            case "gen-sampleset":
                Generate_SampleSet.main(argv); break;
            case "gen-urllist":
                Generate_URLList.main(argv); break;
            case "run-sample":
            default: // fallthrough
                Test_Sample.main(argv); break;
        }
    }
} // Main
