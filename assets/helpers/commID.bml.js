//
// Bookmarklet for appending the comment headers
// with the comment ID and index number. To enable,
// copy the JavaScript code into browser with
// "javascript:" (without quotes) at the beginning
// of URL. Save the URL as bookmark.
//
(function ($) {
    $("div[id^=comment-]").each(function (i, elem) {
        var head = $(elem).find(".verbose .action-head .action-details");
        head.css({"color":"red", "text-transform":"uppercase"})
            .text(head.text() + " = " + elem.id + " - [" + (i + 1) + "]");
    });
}(jQuery));
