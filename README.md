EECS 4080 - Mining Bug Reports Project
==========

## Getting Started

To run the main application:

```
#!bash

    java -Djava.library.path=libs/hunspell.jar \
         -classpath "bin/production/eecs4080;
                     libs/hunspell.jar;
                     libs/jazzy.jar;
                     libs/jortho.jar;
                     libs/joox.jar;
                     libs/jna-platform.jar;
                     libs/jna.jar" Main <cmd<args>
```

## Commands

### test-io

Test load/store of bug reports

  * `infile` - Input bug report XML file
  * `outfile` - Output Phase 0 XML file

### test-p1

Test Phase 1 transformation of bug reports

  * `infile` - Input bug report XML file
  * `outfile` - Output Phase 1 XML file

### test-p2

Test Phase 2 transformation of bug reports

  * `infile` - Input bug report XML file
  * `outfile` - Output Phase 2 XML file

### run-sample

Run algorithm on sample dataset

### copy-sample

Copy sample dataset into sample directory

  * `filemap` - File with list of file paths to bug reports
  * `samplelist` - File with list of sample dataset bug reports

### gen-kwlist

Generate programming language reserved keyword list

### gen-sampleset

Generate sample set of given size

  * `filelist` - File with list of file paths to bug reports
  * `size` - Number of sample bug reports to be included
  * `outfile` - File to write output result

### gen-urllist

Generate list of URL of bug reports

  * `dirpath` - Root base directory path containing all bug reports
  * `outfile` - File path to output file to write to

## Program Organization

The program source code is organized and layout into several
packages:

  * `classifier`
  * `helpers`
  * `loader`
  * `storer`
  * `structs`
  * `tests`
  * `transform`
  * `util`

The guts of the algorithm are in `classifier` and `transform`,
while the `loader` and `storer` packages hold the code for reading
and writing the input and output of the algorithm at different stages.
The `structs` package contains data structures for the algorithm and
`util` contains helper functions for common tasks.

The `tests` package holds the main classes for running the tests.
The `helpers` are the classes that help generate various lists,
components, and inputs for the algorithm and tests, and perform
other tasks that prepare the input data for the tester.

## Libraries

  * `hunspell.jar`     (https://github.com/dren-dk/HunspellJNA)
  * `jazzy.jar`        (http://sourceforge.net/projects/jazzy/)
  * `jna-platform.jar` (https://github.com/java-native-access/jna)
  * `jna.jar`          (https://github.com/java-native-access/jna)
  * `joox.jar`         (https://github.com/jOOQ/jOOX)
  * `jortho.jar`       (http://sourceforge.net/projects/jortho)

  > **Note:**

  > JOrtho was modified to give access to internal APIs, namely:
    `public` access modifier were added to the `DictionaryFactory`
    and `Dictionary` classes.

## Assets

  * **Dictionaries**

    * `hunspell`      (http://sourceforge.net/projects/hunspell/files/Spelling%20dictionaries/en_US/)
    * `jazzy`         (http://sourceforge.net/projects/jazzy/files/Dictionaries/English/)
    * `jOrtho`        (http://download.wikimedia.org/enwiktionary/latest/enwiktionary-latest-pages-articles.xml.bz2)

          `java -Xmx256M -cp jortho.jar com.inet.jorthodictionaries.BookGenerator en dictionary`

    * `keywords`      (https://wiki.gnome.org/Projects/GtkSourceView/)

```
#!bash

        git clone git://git.gnome.org/gtksourceview
        cp gtksourceview/data/language-specs assets/keywords/gtksourceview/language-specs
        java -Djava.library.path=libs/hunspell.jar \
           -classpath "bin/production/eecs4080;
                       libs/hunspell.jar;
                       libs/jazzy.jar;
                       libs/jortho.jar;
                       libs/joox.jar;
                       libs/jna-platform.jar;
                       libs/jna.jar" Main gen-kwlist
        rm -fr gtksourceview
```

  * **Helpers**

    * `commID.bml.js`:
      JavaScript bookmarklet for appending the comment headers
      with the comment ID and index number.

## Tests

  * Supervised - The development testing bug reports
  * Sample - The sample data set
